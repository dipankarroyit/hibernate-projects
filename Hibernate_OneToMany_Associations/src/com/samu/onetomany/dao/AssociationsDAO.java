package com.samu.onetomany.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.samu.onetomany.dto.Brand;

public class AssociationsDAO {

	
	public void saveBrandDetails(Brand brand) {
		Configuration configuration = new Configuration();
		configuration.configure("config.xml");
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(brand);
		transaction.commit();	
	}
}
