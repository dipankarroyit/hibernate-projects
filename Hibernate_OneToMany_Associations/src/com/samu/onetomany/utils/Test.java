package com.samu.onetomany.utils;

import java.util.ArrayList;
import java.util.List;
import com.samu.onetomany.dao.AssociationsDAO;
import com.samu.onetomany.dto.Brand;
import com.samu.onetomany.dto.Model;

public class Test {

	public static void main(String[] args) {

		Model model1 = new Model("iphone10",62500.500D , "ios,5.8inches,12megapixel", 3.0D);
		Model model2 = new Model("iphoneSE",32500.500D , "ios,4.8inches,15megapixel", 3.2D);
		ArrayList<Model> list = new ArrayList<Model>();
		
		list.add(model1);
		list.add(model2);
		
		Brand brand = new Brand();
		brand.setName("Apple");
		brand.setOrigin("usa");
		brand.setSince("1976");
		brand.setAmbassdor("steve jobs");
		brand.setModel(list);
		
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveBrandDetails(brand);
		
	}

}
