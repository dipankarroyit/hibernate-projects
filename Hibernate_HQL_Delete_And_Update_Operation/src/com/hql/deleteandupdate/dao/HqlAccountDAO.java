package com.hql.deleteandupdate.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.hql.deleteandupdate.dto.AccountDTO;

public class HqlAccountDAO {
	
	public void updateAddressByAccountNumber(String accountNumber,String newAddress) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="update AccountDTO set address=:newAddress where accountNumber=:accountNumber";
		Query query = session.createQuery(hql);
		query.setParameter("newAddress", newAddress);
		query.setParameter("accountNumber", accountNumber);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Update Operation Failed");
			return;
		}
		System.out.println("Update Operation successfull");
	}
	
	public void deleteById(Long id) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="delete AccountDTO where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Delete Operation Failed");
			return;
		}
		System.out.println("Delete Operation successfull");
	}
}
