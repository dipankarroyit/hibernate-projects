package com.hql.deleteandupdate.utils;

import java.util.List;

import com.hql.deleteandupdate.dao.HqlAccountDAO;
import com.hql.deleteandupdate.dto.AccountDTO;

public class Test {

	public static void main(String[] args) {
		
		HqlAccountDAO hqlAccountDAO = new HqlAccountDAO();
		
		//hql update operation
		hqlAccountDAO.updateAddressByAccountNumber("IND133", "Bangalore, India");
		
		//hql delete operation
		hqlAccountDAO.deleteById(101L);
	}

}
