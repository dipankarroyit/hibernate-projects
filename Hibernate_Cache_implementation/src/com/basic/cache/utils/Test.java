package com.basic.cache.utils;

import java.util.ArrayList;
import java.util.List;

import com.basic.cache.dao.AccountDAO;
import com.basic.cache.dao.HqlAccountDAO;
import com.basic.cache.dto.AccountDTO;

public class Test {

	public static void main(String[] args) {
	
		HqlAccountDAO accountDAO = new HqlAccountDAO();
		List<AccountDTO> accountDetails1 = accountDAO.getAccountDetails();
		List<AccountDTO> accountDetails2 = accountDAO.getAccountDetails();
		System.out.println("size of accountDetails1 is "+accountDetails1.size());
		System.out.println("size of accountDetails2 is "+accountDetails2.size());
	}

}

