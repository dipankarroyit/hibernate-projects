package com.hibernate.basics.utils;

import com.hibernate.basics.dao.AccountDAO;
import com.hibernate.basics.dto.AccountDTO;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		AccountDTO accountDTO = new AccountDTO();
		accountDTO.setId(101L);
		accountDTO.setMobileNumber(78561235L);
		accountDTO.setAccountHodlerName("Dipankar");
		accountDTO.setAccountNumber("IND133");
		accountDTO.setAddress("India");
		accountDTO.setBalance(10202.5D);
		
		AccountDAO dao = new AccountDAO();
		dao.saveAccountDetails(accountDTO);
	}

}

