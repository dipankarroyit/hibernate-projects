package com.samu.onetoone.utils;

import java.util.List;
import com.samu.onetoone.dao.AssociationsDAO;
import com.samu.onetoone.dto.Captain;
import com.samu.onetoone.dto.Team;

public class Test {

	public static void main(String[] args) {
		
		Captain captain = new Captain();
		captain.setName("M.S Dhoni");
		captain.setAge(39L);
		captain.setTotalExperience(15L);
		captain.setSkills("wicketkeepr,batsman");
		
		Team team = new Team();
		team.setName("India");
		team.setSize(11L);
		team.setType("cricket");
		team.setJerseyColor("blue");
		team.setCaptain(captain);
		
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveTeamDetails(team);
	}
}
