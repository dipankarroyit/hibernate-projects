package com.samu.onetoone.dao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.samu.onetoone.dto.Team;

public class AssociationsDAO {

	public void saveTeamDetails(Team team) {
		Configuration configuration = new Configuration();
		configuration.configure("config.xml");
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(team);
		transaction.commit();	
	}
}
