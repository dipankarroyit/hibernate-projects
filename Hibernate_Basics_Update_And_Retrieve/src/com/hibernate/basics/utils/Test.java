package com.hibernate.basics.utils;

import com.hibernate.basics.dao.AccountDAO;
import com.hibernate.basics.dto.AccountDTO;

public class Test {

	public static void main(String[] args) {
		
		// Retrieve operation
		AccountDAO dao = new AccountDAO();
	
		AccountDTO accountDTO = dao.getAcoundDetailsById(101L);
		if(accountDTO != null) {
			System.out.println(accountDTO);
		}else {
			System.out.println("Invaid Id");
		}
		
		// Update operation	
		dao.updateMobileNumberById(101L, 8951606106L);
	}

}
