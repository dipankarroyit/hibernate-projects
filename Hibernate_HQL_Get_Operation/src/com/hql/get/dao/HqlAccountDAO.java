package com.hql.get.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hql.get.dto.AccountDTO;

public class HqlAccountDAO {

	
	public List<AccountDTO> getAccountDetails() {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from AccountDTO";
		Query query = session.createQuery(hql);
		List<AccountDTO> list = query.list();
		return list;
	}
	
	public AccountDTO getAccountDetailsByAccountNumber(String accountNumber) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql="from AccountDTO where accountNumber=:aNumber";
		Query query = session.createQuery(hql);
		query.setParameter("aNumber", accountNumber );
		AccountDTO uniqueResult = (AccountDTO) query.uniqueResult();
		return uniqueResult;
	}
	
}
