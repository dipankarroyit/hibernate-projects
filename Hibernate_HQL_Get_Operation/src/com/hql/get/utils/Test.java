package com.hql.get.utils;

import java.util.List;

import com.hql.get.dao.HqlAccountDAO;
import com.hql.get.dto.AccountDTO;

public class Test {

	public static void main(String[] args) {
		
		HqlAccountDAO hqlAccountDAO = new HqlAccountDAO();
		
		// list get operation using hql
		List<AccountDTO> list = hqlAccountDAO.getAccountDetails();
		list.forEach(obj->{
			System.out.println(obj);
		});
		// get operation using hql
		AccountDTO accountDTO = hqlAccountDAO.getAccountDetailsByAccountNumber("IND133");
		System.out.println(accountDTO);
	}

}
