package com.samu.manytoone.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.samu.manytoone.dto.Apps;

public class AssociationsDAO {

	public void saveAppsDetails(Apps apps) {
		Configuration configuration = new Configuration();
		configuration.configure("config.xml");
	 	SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(apps);
		transaction.commit();	
	}
	
	
}
