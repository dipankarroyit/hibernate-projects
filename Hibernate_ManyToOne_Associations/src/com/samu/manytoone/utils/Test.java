package com.samu.manytoone.utils;

import java.util.ArrayList;
import java.util.List;
import com.samu.manytoone.dao.AssociationsDAO;
import com.samu.manytoone.dto.Apps;
import com.samu.manytoone.dto.System;

public class Test {

	public static void main(String[] args) {
		
		System system = new System();
		system.setName("Hp");
		system.setOsType("windows");
		system.setVersion(10.1);
		
		Apps Apps = new Apps();
		Apps.setName("sts");
		Apps.setSize("310mb");
		Apps.setVersion("4.0");
		Apps.setSystem(system);
		
		
		
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveAppsDetails(Apps);
	}

}
